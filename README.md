# Lab04

Modularity and error handling using `either` and `maybe`

## Learning objectives for Lab04, as outlined by lecturer

> ### Objectives for Lab04
> - Basics of error handling with Maybe and Either types
> - understand the code, try not to use ChatGPT

----------------

## The task - decoding an intergalactic message (a sequence of numbers)

This is a modular program that takes a string input, and finds (if possible) the secret message.

The **requirements**:
1. Unique Min-Max
    - The min and max number of the sequence needs to be unique
    - Displays relevant error message when they aren't
2. Even Sum
    - The sum of the min and max numbers needs to be an even number
3. Count Occurrence of Avg
    - The average ((min + max) / 2) number is the "magic number", and the number of occurrences of this magic number in the number sequence is the "secret message"

All functions that should logically have a doctest, has one.

### Brief Description of Functions Implemented

#### `toList`

Parses a string to a list of integers, using `map`, `read` and `words`

#### `countOcc`

Counts occurrences of value `a` in a list of values `[a]`, returns the number of occurrences found

#### `isUnique`

Returns true or false depending on whether a number occurs only once or not in a list (i.e., if it's unique)

#### `isEven`

This one is a self-implementation of the `even` function from the std library. Left it in the project as I only found the std lib one after having implemented it. 
Returns true or false depending on whether a number is even or not

#### `findMagicNumber`

Finds the magic number, which is the sum of min and max divided by 2, or otherwise returns 0 if this number is not even using guards

#### `printLst`

A recursive formatting function to test conversion of int list to a formatted string, not needed

*and finally*

#### `decodeMessage`

Implementation using all the previously defined functions put together to produce the final secret message from an input string. 
The function uses:
- let..in..
- Maybe
- if..then..else


*Output in main with number sequence = "5 5 5 8 1 2 3 4 9 8 2 3 4 8"*
```
The min and max numbers 1 and 9 are Unique!
The min number is 1 is Unique!
The sum 10 is Even
Magic number is 5
The message is 3!!
The message is Just 3
```
