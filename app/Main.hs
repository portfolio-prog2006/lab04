  {-
module Main (main) where

import Lib

main :: IO ()
main = do



    msg <- getContents
    case decodeMessage msg of
        Just n -> putStrLn $ "The message is " ++ show n ++ " characters long."
        Nothing -> putStrLn "The message cannot be decoded. Interference detected."
-}



module Main (main) where

import Lib
import Data.List

main :: IO ()
main = do

-- Arbitrary message (I'm testing output while creating the functions)
   let s = "5 5 5 8 1 2 3 4 9 8 2 3 4 8"
   let xxs = toList s
   let maxNum = maximum xxs
   let minNum = minimum xxs
   let sumMinMax = maxNum + minNum
{-
-- First, using the individually created functions
  putStrLn "\n\tUsing the individually created functions:\n"
-- Checking for unique Min and Max
   putStr $ "The min and max numbers " ++ show minNum ++ " and " ++ show maxNum
   if isUnique minNum xxs && isUnique maxNum xxs
       then putStrLn " are Unique!"
   else putStrLn " are not Unique"

-- Checking for unique Min and Max
   putStr $ "The min number is " ++ show minNum
   if isUnique minNum xxs
       then putStrLn " is Unique!"
   else putStrLn " is not Unique"

-- Checking if the sum of min+max is an even number
   putStr $ "The sum " ++ show sumMinMax
   if isEven sumMinMax
       then putStrLn " is Even"
   else putStrLn " is Odd"

-- Min + Max divided by 2: The Magic Number
   let magicNum = findMagicNumber sumMinMax
   if isEven sumMinMax
       then putStrLn $ "Magic number is " ++ show magicNum
   else putStrLn "The magic number doesn't exist, sum is not even"

-- If there's a magic number (sum is even, magicNum /= 0)
   let message = countOcc magicNum xxs
   if magicNum /= 0
       then putStrLn $ "The message is " ++ show message ++ "!!"
   else putStrLn "No message!"
-}

-- Final function 'decodeMessage' on a valid msg input
   putStrLn "\n\tUsing the decodeMessage composite function:\n"
   putStrLn $ "The message is " ++ show (decodeMessage s)

