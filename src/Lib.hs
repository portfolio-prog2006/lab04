module Lib
    (
    printLst,
    countOcc,
    isEven,
    isUnique,
    findMagicNumber,
    toList,
    decodeMessage
    ) where

import Text.Read
import Data.List

-- | Decode an intergalactic message from a string.
-- The message is a sequence of integers separated by spaces.
--
-- >>> decodeMessage "5 5 5 8 1 2 3 4 9 8 2 3 4 1"
-- Nothing
--
-- >>> decodeMessage "5 5 5 8 1 2 3 4 9 8 2 3 4 9"
-- Nothing
--
-- >>> decodeMessage "5 5 5 8 1 2 3 4 9 8 2 3 4 8"
-- Just 3
--
-- >>> decodeMessage "5 5 5 1 2 3 4 8 2 3"
-- Nothing
decodeMessage :: String -> Maybe Int
decodeMessage msg =
    case toList msg of
        [] -> Nothing
        list ->
            let minNum = minimum list
                maxNum = maximum list
                sumNum = minNum + maxNum
                magicNumber = findMagicNumber sumNum    -- Returns 0 if sumNum is Odd!
                message = countOcc magicNumber list     -- The message to be returned if checks are passed
            in
                if even sumNum && isUnique minNum list && isUnique maxNum list
                    then    Just message    -- message is found!
                else        Nothing         -- magicNumber is not in the list


-- | Parses a string to a list of integers
--
-- >>> toList "1 2 3"
-- [1,2,3]
-- >>> toList "4 5 6"
-- [4,5,6]
toList :: String -> [Int]
toList xs = map read $ words xs

-- | Counts occurrences of a value a in a list of values [a], returns the number of occurrences found
--
-- >>> countOcc 3 [1, 2, 3, 3, 4, 5]
-- 2
-- >>> countOcc 6 [1, 2, 3, 3, 4, 5]
-- 0
countOcc :: Ord a => a -> [a] -> Int
countOcc _ [] = 0
countOcc x xs = (length . filter (== x)) xs

-- | Returns whether a number occurs only once or not in a list (i.e., if it's unique)
--
-- >>> isUnique 3 [1, 2, 3, 4, 5]
-- True
-- >>> isUnique 3 [3, 3, 3, 4, 5]
-- False
isUnique :: Int -> [Int] -> Bool
isUnique n xs
    | countOcc n xs == 1  = True
    | otherwise           = False

-- | Redundant! Found the standard library function 'even' after I made this one
--
-- >>> isEven 4
-- True
-- >>> isEven 7
-- False
isEven :: Int -> Bool
isEven n
    | n `mod` 2 == 0    = True
    | otherwise         = False

-- | Finds the magic number (sum of min and max div by 2), or returns 0 if it's odd as a safe-quard
--
-- >>> findMagicNumber 6
-- 3
-- >>> findMagicNumber 5
-- 0
findMagicNumber :: Int -> Int
findMagicNumber n
    | even n        = n `div` 2
    | otherwise     = 0

-- | Just a recursive formatting function to test conversion of int list to a formatted string, not needed or used :)
--
-- >>> printLst [1, 2, 3]
-- "1 2 3"
-- >>> printLst []
-- ""
-- >>> printLst [42]
-- "42"
printLst :: [Int] -> String
printLst [] = ""
printLst [x] = (show x)
printLst (x:xs) = (show x) ++ " " ++ printLst xs
